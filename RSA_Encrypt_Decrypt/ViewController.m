//
//  ViewController.m
//  RSA_Encrypt_Decrypt
//
//  Created by sban@netspectrum.com on 9/25/12.
//  Copyright (c) 2012 sban@netspectrum.com. All rights reserved.
//

#import "ViewController.h"
#import "RSA.h"

@interface ViewController ()
@property (nonatomic,strong)IBOutlet UITextField *enField;
@property (nonatomic,strong)IBOutlet UITextField *deField;
@property (nonatomic,strong)IBOutlet UIButton *generateBtn;
@property (nonatomic,strong)IBOutlet UIActivityIndicatorView *activeIndicator;

@end

@implementation ViewController
@synthesize enField,deField,generateBtn;
@synthesize activeIndicator;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [enField becomeFirstResponder];
    deField.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)encrypt:(id)sender{
    [activeIndicator startAnimating];
    
    RSA *rsa = [RSA shareInstance];
    [rsa generateKeyPairRSACompleteBlock:^{
        if (enField.text) {
            //encrypt
            NSData *encryptData = [rsa RSA_EncryptUsingPublicKeyWithData:[enField.text dataUsingEncoding:NSUTF8StringEncoding]];
            
            //decrypt
            NSData *decryptData = [rsa RSA_DecryptUsingPrivateKeyWithData:encryptData];
            NSString *originString = [[NSString alloc] initWithData:decryptData encoding:NSUTF8StringEncoding];
            
            deField.text = originString;
            
            NSLog(@"---- public key bits %i",[rsa.publicKeyBits length]);
            
            [activeIndicator stopAnimating];
        }
    }];
}
@end
