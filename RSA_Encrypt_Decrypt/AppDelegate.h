//
//  AppDelegate.h
//  RSA_Encrypt_Decrypt
//
//  Created by sban@netspectrum.com on 9/25/12.
//  Copyright (c) 2012 sban@netspectrum.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
